$(function(){
    // Storing some elements in variables
    var shoutBox = $('.shoutbox'),
        form = shoutBox.find('form'),
        inputName = form.find('#input_name'),
        inputEmail = form.find('#input_email'),
        inputContent = form.find('#input_content');
        btnShout = shoutBox.find('#btn_shout');
        ulListMessage = shoutBox.find('.list-message');
        lastLiMessageItem = shoutBox.find('.list-group-item:last');
        alertSuccess = shoutBox.find('.alert-success');

    let scrolltoh = ulListMessage[0].scrollHeight;
        ulListMessage.scrollTop(scrolltoh);

    var validatePassed = true;
    var currentLastMessageID = lastLiMessageItem.attr('data-id');

    // Automatically update messages every 5 seconds
    setInterval(reloadMessageList,5000);

    // Handle Shout btn click
    btnShout.click(function(e){
        e.preventDefault();

        var name = inputName.val().trim();
        var email = inputEmail.val().trim();
        var content = inputContent.val().trim();

        //Validate inputs
        if(!validateInput(inputName)){
            validatePassed = false;
        }
        if(!validateInput(inputEmail, true)){
            validatePassed = false;
        }
        if(!validateInput(inputContent)){
            validatePassed = false;
        }

        if(!validatePassed){
            validatePassed = true;
            return;
        }

        // If validate pass, save message to the database.
        if(validatePassed === true) {
            saveMessage(name, email, content);
        }
    });

    // Add message to the database
    function saveMessage(name,email,content){
        showLoading();
        $.ajax({
            type:"post",
            url:base_url + '/app/request.php',
            data:{name: name, email: email, content: content},
            success: function() {
                inputName.val("");
                inputEmail.val("");
                inputContent.val("");
                hideLoading();
                reloadMessageList();
                alertSuccess.show();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                console.log( 'The following error occured: ' + textStatus, errorThrown );
            }
        });
    }

    // Reload message list
    function reloadMessageList(){
        $.ajax({
            type:"get",
            url:base_url + '/app/request.php',
            data:{getMessages: true},
            dataType : "json",
            success: function(response) {
                console.log(response.lastMessageID);
                // If there are any new messages
                if(currentLastMessageID < response.lastMessageID) {
                    // Load new message
                    ulListMessage.empty();
                    ulListMessage.append(response.messages);
                    // Scroll to latest message
                    let scrolltoh = ulListMessage[0].scrollHeight;
                    ulListMessage.scrollTop(scrolltoh);
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                console.log( 'The following error occured: ' + textStatus, errorThrown );
            }
        });
    }

    // Validate date input date
    function validateInput(inputElement, isValidateEmail = false){
        let data = inputElement.val().trim();
        if(data === ""){
            showInputError(inputElement);
            return false;
        }

        if(isValidateEmail === true){
            if(!isEmail(data)){
                showInputError(inputElement);
                return false;
            }
        }

        hideInputError(inputElement);
        return true;
    }

    // Validate email
    function isEmail(input) {
        var validRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        if (input.match(validRegex)) {
            return true;
        } else {
            return false;
        }
    }

    function showInputError(inputElement){
        inputElement.parents('.form-group').addClass('has-error');
        inputElement.parents('.form-group').find('.help-block').show();
    }

    function hideInputError(inputElement){
        inputElement.parents('.form-group').removeClass('has-error');
        inputElement.parents('.form-group').find('.help-block').hide();
    }

    function showLoading(){
        btnShout.css('cursor','wait');
        btnShout.find('.loader').show();
        btnShout.find('#btn_text_loading').show();
        btnShout.find('#btn_text_shout').hide();

        inputName.attr('disabled','disabled');
        inputEmail.attr('disabled','disabled');
        inputContent.attr('disabled','disabled');
    }

    function hideLoading(){
        btnShout.css('cursor','auto');
        btnShout.find('.loader').hide();
        btnShout.find('#btn_text_loading').hide();
        btnShout.find('#btn_text_shout').show();

        inputName.prop("disabled", false);
        inputEmail.prop("disabled", false);
        inputContent.prop("disabled", false);
    }

    $('.alert-success').find('.close').click(function (){
        $(this).parents('.alert-success').hide();
    });
});