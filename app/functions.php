<?php
require_once "Message_model.php";
require_once "vendor/autoload.php";
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Generate list messages HTML from data
function messagesHTML(array $messages)
{
    if(empty($messages)){
        return;
    }
    $messages_html = "";
    for ($i=count($messages)-1; $i>=0; $i--) {
        $messages_html .= '<li class="list-group-item" data-id="'.$messages[$i]['id'].'">
            <div class="media">
                <div class="media-left" style="display: none">
                      <span class="avatar avatar-online">
                        <img src="https://bootdey.com/img/Content/avatar/avatar3.png" alt="">
                      </span>
                </div>
                <div class="media-body">
                    <h5 class="list-group-item-heading">
                        <small class="pull-right">'.showDateTime($messages[$i]['created_at']).'</small>
                        <span class="m_name">'. $messages[$i]['name'].'</span> <span class="m_email">'.$messages[$i]['email'].'</span>
                    </h5>
                    <p class="list-group-item-text">'.$messages[$i]['content'].'</p>
                </div>
            </div>
        </li>';
    }

    return $messages_html;
}

// Format date and time
function showDateTime($time_stamp)
{
    return date("H:i d M Y", strtotime($time_stamp));
}

// Filter input data for security purpose
function inputFilter($input)
{
    $data = htmlspecialchars($input);
    $data = str_replace(array("\n", "\r"), '', $data);

    return $data;
}

// Send email to admin
function sendMailToAdmin($name, $email, $content)
{
    $mail = new PHPMailer(true);
    try {
        //Server settings
        //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
        $mail->isSMTP();                                            //Send using SMTP
        $mail->Host       = SMTP_SERVER;                            //Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = EMAIL_USER;                             //SMTP username
        $mail->Password   = EMAIL_PASSWORD;                         //SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
        $mail->Port       = SMTP_PORT;                              //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

        //Recipients
        $mail->setFrom('from@example.com', 'Shout box');
        $mail->addAddress(ADMIN_EMAIL, 'Admin');     //Add a recipient
        $mail->addReplyTo('info@example.com', 'Information');

        //Content
        $mail->isHTML(true);                                  //Set email format to HTML
        $mail->Subject = 'Shout box - New shout!';

        $body = "We have new shout: | 
                 Name: ".$name." | ".
                "Email: ".$email." | ".
                "Content: ".$content;

        $mail->Body    = $body;
        $mail->AltBody = $body;

        $mail->send();

        return true; // Mail sent
    } catch (Exception $e) {
        return false;
    }
}

// Dump data to debug
function dd($var)
{
    if(is_bool($var) || empty($var)){
        var_dump($var);
    }else {
        echo "<pre>";
        print_r($var);
        echo "</pre>";
    }
}

