<?php
require_once "functions.php";

/**
 * Handle ajax add message request
 */
if(isset($_POST["name"]) && isset($_POST["email"]) && isset($_POST['content'])) {
    $name = inputFilter($_POST["name"]);
    $email = inputFilter($_POST["email"]);
    $content = inputFilter($_POST["content"]);

    // Storing a new shout
    $Message = new Message_model();
    $message_id = $Message->add($name, $email, $content);

    // Send email to admin
    if(isset($message_id) && $message_id > 0){
        sendMailToAdmin($name, $email, $content);
    }

    echo json_encode([
        'message_id' => $message_id,
        'success' => true
    ]);
}

/**
 * Handle ajax update message list request
 */
if(isset($_GET['getMessages']) && $_GET['getMessages']){
    $Message = new Message_model();
    $messages = $Message->get(50);
    $lastMessageID = $messages[0]['id'];
    $messagesHTML = messagesHTML($messages);

    echo json_encode([
        'messages' => $messagesHTML,
        'lastMessageID' => $lastMessageID,
        'success' => true
    ]);
}

/**
 * Get last 50 messages
 */
function getTheMessages()
{
    $Message = new Message_model();
    $messages = $Message->get(50);

    return messagesHTML($messages);
}




