<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Shout box - Duy Nguyen</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="<?php echo BASE_URL; ?>/assets/js/jquery-1.10.2.min.js"></script>
    <link href="<?php echo BASE_URL; ?>/assets/bootstrap/bootstrap.min.css" rel="stylesheet">
    <script src="<?php echo BASE_URL; ?>/assets/bootstrap/bootstrap.min.js"></script>

    <link href="<?php echo BASE_URL; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo BASE_URL; ?>/assets/css/style.css" rel="stylesheet">
    <script type="text/javascript">
        var base_url = '<?php echo BASE_URL; ?>';
    </script>
</head>
<body>