<?php
require_once "config.php";

class Message_model {
    protected $_table = "message";
    protected $db;

    public function __construct()
    {
        global $pdo;
        $this->db = $pdo;
    }

    public function get($limit = 50)
    {
        $sql = "SELECT id, name, email, content, created_at FROM ".$this->_table." order by id desc limit $limit;";
        $result = $this->db->query($sql);

        return $result->fetchAll();
    }

    public function add($name, $email, $content)
    {
        $sql = "INSERT INTO ".$this->_table." (name, email, content) VALUES (:name, :email, :content)";

        if($stmt = $this->db->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":name", $name, PDO::PARAM_STR);
            $stmt->bindParam(":email", $email, PDO::PARAM_STR);
            $stmt->bindParam(":content", $content, PDO::PARAM_STR);

            $rs = $stmt->execute();
            // Close statement
            unset($stmt);
        }

        return $this->db->lastInsertId();
    }
}

