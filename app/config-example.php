<?php
/* Application Home/Base url */
define('BASE_URL', 'http://localhost/shout-box');

/* Database configurations */
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_NAME', 'shout-box');

/* Email sender */
define('SMTP_SERVER', 'smtp.gmail.com.');
define('SMTP_PORT', 465);
define('EMAIL_USER', 'youremail@gmail.com');
define('EMAIL_PASSWORD', 'yourpassword');

/* Admin email, to receive new shout email notification */
define('ADMIN_EMAIL', 'adminemail@gmail.com');


/* Attempt to connect to MySQL database */
try{
    $pdo = new PDO("mysql:host=" . DB_SERVER . ";dbname=" . DB_NAME, DB_USERNAME, DB_PASSWORD);
    // Set the PDO error mode to exception
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e){
    die("ERROR: Could not connect. " . $e->getMessage());
}