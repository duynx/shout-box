<?php
require_once "app/request.php";
require_once "app/views/header.php";

?>
<div class="row bootstrap snippets bootdeys">
    <div class="col-md-6 col-xs-12 col-md-offset-3">
        <div class="panel shoutbox" id="messge">
            <div class="panel-heading">
                <h3 class="panel-title">Shout box</h3>
            </div>
            <div class="panel-body">
                <ul class="list-group list-group-full list-message">
                    <?php echo getTheMessages(); ?>
                </ul>
            </div>
            <div class="panel-footer">
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Done!</strong> Send message successfully.
                </div>
                <form>
                    <div class="form-group">
                        <label class="control-label" for="input_name">Name</label>
                        <input type="text" class="form-control" id="input_name" name="name">
                        <span class="help-block">Please enter your name</span>
                    </div>
                    <div class="form-group"> <!-- has-error -->
                        <label class="control-label" for="input_email">Email</label>
                        <input type="text" class="form-control" id="input_email" name="email">
                        <span class="help-block">Please enter a valid email</span>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="input_content">Message</label>
                        <textarea class="form-control" id="input_content" name="content" rows="3"></textarea>
                        <span class="help-block">Please enter your message</span>
                    </div>
                    <a id="btn_shout" href="#!">
                        <div class="loader"></div>
                        <span id="btn_text_loading">sending...</span>
                        <span id="btn_text_shout">Shout</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>
<?php require_once "app/views/footer.php"; ?>