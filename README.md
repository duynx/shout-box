# Shout box - Duy Nguyen

This is a simple Shout box using pure PHP, jQuery, Bootstrap 3.

### Included functions:

1. Send message to Shout box.

2. Validate from data (empty and email validate).

3. Status message whether the validation was successful or an error occurred.

4. Sending an email to the administrator with the form data after successful saving into the database.

5. Save data with Ajax 
   
6. Auto update with Ajax every 5 seconds

### Installation

1. Import `db.sql` to your mysql database

2. Change sql connection informations in `app/config.php`, required to update:
    
   _If dont have `app/config.php` file yet. Change `app/config-example.php` to `app/config.php`_

- Base URL

- Database connection

- Email sender information, to able to send email.

- Admin email, to receive new shout email.

3. Run `composer install` inside folder `app`. If fodler `vendor` don't exist yet.


4. Run the code (default to `index.php`)